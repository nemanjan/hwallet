# https://gitlab.com/nemanjan/hwallet
# Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import serial
import struct
import binascii

PKG_DATA_SIZE = 32

MSG_MAGIC = 0xDEDABABA
MSG_BITCOIN = 1<<8

BTC_KEYPAIR_GET = MSG_BITCOIN + 0
BTC_KEYPAIR_SET = MSG_BITCOIN + 1
BTC_KEYPAIR_GENERATE = MSG_BITCOIN + 2
BTC_INIT_TX = MSG_BITCOIN + 3
BTC_ADD_INPUT = MSG_BITCOIN + 4
BTC_ADD_OUTPUT = MSG_BITCOIN + 5
BTC_SIGN_TX = MSG_BITCOIN + 6

RSP_MAGIC = struct.pack('<I', 0xBABADEDA)
RSP_ACK = 0
RSP_NAK = 1
RSP_PRIVKEY = 2
RSP_PUBKEY = 3
RSP_SIGNATURE_R = 4
RSP_SIGNATURE_S = 5
RSP_TX = 6

class HWallet(object):
    def __init__(self, address='/dev/ttyACM0', baudrate=115200, timeout=.2):
        self.wallet = serial.Serial(address, baudrate=baudrate, timeout=timeout)
        self.wallet.read(10000) # flush device

    def __del__(self):
        try:
            if self.wallet.is_open:
                self.wallet.close()
        except:
            pass

    def send(self, msgtype, data=None):
        if data is None:
            data = b'\x00'*PKG_DATA_SIZE
        length = len(data)
        msgnum = int(length/PKG_DATA_SIZE)
        if length % PKG_DATA_SIZE:
            msgnum +=1
        for i in range(msgnum):
            left = length - PKG_DATA_SIZE * i
            if i == (msgnum - 1):
                msglen = left
            else:
                msglen = PKG_DATA_SIZE
            rawmsg = struct.pack('<HH%ds'%PKG_DATA_SIZE, msgtype, left,
                    data[PKG_DATA_SIZE*i : PKG_DATA_SIZE*i + msglen] +
                    b'\x00' * (PKG_DATA_SIZE-msglen))
            msg = struct.pack('<I%dsI'%(PKG_DATA_SIZE+4), MSG_MAGIC, rawmsg,
                              binascii.crc32(rawmsg) & 0xffffffff)
            #print('Sending: %s'%msg.hex())
            self.wallet.write(msg)

            if i != (msgnum-1):
                if self.receive()[0]['type'] == RSP_NAK:
                    raise Exception('Received NAK')

    def receive(self, wait=False):
        rsps = []
        if wait:
            tmptimeout = self.wallet.timeout
            self.wallet.timeout = None
            rcvd = self.wallet.read(1)
            self.wallet.timeout = tmptimeout
            rcvd += self.wallet.read(10000)
        else:
            rcvd = self.wallet.read(10000)
        index = rcvd.find(RSP_MAGIC)
        # print(rcvd.hex())

        to_rcv = 0
        while index >= 0:
            if len(rcvd[index:]) < (PKG_DATA_SIZE+12):
                break
            rawpkt = rcvd[index+4:index+PKG_DATA_SIZE+12]
            pkt = struct.unpack('<%dsI'%(PKG_DATA_SIZE+4), rawpkt)
            crc = binascii.crc32(pkt[0]) & 0xffffffff
            if crc == pkt[1]:
                typ, length, rspdata = struct.unpack('<HH%ds'%PKG_DATA_SIZE, pkt[0])
                # print('Type: %d\nLen: %d\nData: %s\nto_rcv: %d'% (typ, length, rspdata.hex(),to_rcv))
                consumed = False
                if to_rcv > 0:
                    if rsps[-1]['type'] == typ and length == to_rcv:
                        if length < PKG_DATA_SIZE:
                            rsps[-1]['data'] += rspdata[:length]
                            to_rcv -= length
                        else:
                            rsps[-1]['data'] += rspdata
                            to_rcv -= PKG_DATA_SIZE
                        consumed = True
                    else:
                        print('Possible transmission error')
                        to_rcv = 0

                if not consumed:
                    if length > PKG_DATA_SIZE:
                        to_rcv = length - PKG_DATA_SIZE

                    rsps.append({'type': typ, 'length': length, 'data': rspdata})

            rcvd = rcvd[index+PKG_DATA_SIZE+12:]
            index = rcvd.find(RSP_MAGIC)

        return rsps
