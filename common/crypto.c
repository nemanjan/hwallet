//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#include "crypto.h"
#include "clock.h"

const LTC_ECC_Curve secp256k1 = {
    .type = ECC_CURVE_PRIME,
    .p_size = 32,
    .p = { 0x2F, 0xFC, 0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFF,
           0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
           0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
           0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,},
    .a = { 0x00, 0x00, 0x00, 0x00,},
    .b = { 0x07, 0x00, 0x00, 0x00,},
    .Gx = { 0x98, 0x17, 0xF8, 0x16, 0x5B, 0x81, 0xF2, 0x59,
            0xD9, 0x28, 0xCE, 0x2D, 0xDB, 0xFC, 0x9B, 0x02,
            0x07, 0x0B, 0x87, 0xCE, 0x95, 0x62, 0xA0, 0x55,
            0xAC, 0xBB, 0xDC, 0xF9, 0x7E, 0x66, 0xBE, 0x79,},
    .Gy = { 0xB8, 0xD4, 0x10, 0xFB, 0x8F, 0xD0, 0x47, 0x9C,
            0x19, 0x54, 0x85, 0xA6, 0x48, 0xB4, 0x17, 0xFD,
            0xA8, 0x08, 0x11, 0x0E, 0xFC, 0xFB, 0xA4, 0x5D,
            0x65, 0xC4, 0xA3, 0x26, 0x77, 0xDA, 0x3A, 0x48,},
    .n_size = 32,
    .n = { 0x41, 0x41, 0x36, 0xD0, 0x8C, 0x5E, 0xD2, 0xBF,
           0x3B, 0xA0, 0x48, 0xAF, 0xE6, 0xDC, 0xAE, 0xBA,
           0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
           0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,},
};

static uint8_t LTC_Wait(void) {
    uint32_t status = 0;
    while(!(status & (LTC_STATUS_DI | LTC_STATUS_EI))) {
        status = LTC->STATUS;
    }
    if(status & LTC_STATUS_EI) {
        return 1;
    }
    LTC->STATUS |= LTC_STATUS_DI;
    return 0;
}

static void LTC_Write_Reg(volatile uint32_t* addr, const uint8_t* src, uint32_t len) {
    for(uint32_t i=0; i<len; i+=4) {
        uint32_t tmp = 0;
        for(int j=0; j<4; j++)
            tmp |= src[i+j] << (8*j);
        addr[i/4] = tmp;
    }
}

static void LTC_Read_Reg(volatile uint32_t* addr, uint8_t* dst, uint32_t len) {
    for(uint32_t i=0; i<len; i+=4) {
        uint32_t tmp = addr[i/4];
        for(int j=0; j<4; j++)
            dst[i+j] = (tmp >> (8*j)) & 0xFFu;
    }
}

uint8_t CRYPTO_ECDSA_GetPublicKey(const LTC_ECC_Curve* curve,
                                  const uint8_t* priv, uint8_t* pub) {
    // Clear all
    LTC->CW = LTC_CW_ALL;
    // Clear Done Interrupt
    LTC->STATUS |= LTC_STATUS_DI;
    // Set mode to PKHA and clear A,B,N,E memory
    LTC->MODE = LTC_PKHA_CLEARMEM_ABNE;
    LTC_Wait();
    // Set p size
    LTC->N_SIZE = curve->p_size;
    // Write p to N
    LTC_Write_Reg(&LTC->N[0], curve->p, curve->p_size);
    LTC->A_SIZE = curve->p_size;
    // Write a to A3
    LTC_Write_Reg(&LTC->A[16*3], curve->a, 64);
    // Write Gx to A0
    LTC_Write_Reg(&LTC->A[0], curve->Gx, 64);
    // Write Gy to A1
    LTC_Write_Reg(&LTC->A[16], curve->Gy, 64);
    // Write privkey to E
    LTC->E_SIZE = curve->n_size;
    LTC_Write_Reg(&LTC->E[0], priv, curve->n_size);
    // Write b to B0
    LTC->B_SIZE = curve->n_size;
    LTC_Write_Reg(&LTC->B[0], curve->b, 64);
    // Perform multiplication, (B1, B2) = (Vx, Vy)
    LTC->MODE = LTC_PKHA_ECC_MUL_TEQ | curve->type;
    LTC_Wait();
    LTC_Read_Reg(&LTC->B[16], pub+32, 32);
    LTC_Read_Reg(&LTC->B[32], pub, 32);
    return 0;
}

uint8_t CRYPTO_ECDSA_Sign(const LTC_ECC_Curve* curve, const uint8_t* priv,
                          uint8_t* hash, uint32_t len, ECDSA_Signature* sig) {
    // Clear all
    LTC->CW = LTC_CW_ALL;
    // Clear Done Interrupt
    LTC->STATUS |= LTC_STATUS_DI;
    // Set mode to PKHA and clear A,B,N,E memory
    LTC->MODE = LTC_PKHA_CLEARMEM_ABNE;
    LTC_Wait();
    // ================ 1. Calculate u = Nonce mod n ===========================
    // Set N size (page 1249)
    LTC->N_SIZE = curve->n_size;
    // Set Nonce size, same as N
    LTC->A_SIZE = curve->n_size;
    // Write N
    LTC_Write_Reg(&LTC->N[0], curve->n, curve->n_size);
    // Get random Nonce and write to A
    uint8_t nonce[64];
    CRYPTO_Random(nonce, 64);
    LTC_Write_Reg(&LTC->A[0], nonce, curve->n_size);
    // Set mode to AmodN result to A
    LTC->MODE = LTC_PKHA_MOD_AMODN_A;
    LTC_Wait();
    // Save u to nonce
    LTC_Read_Reg(&LTC->A[0], nonce, 64);
    // ================= 2. Calculate h = 1/u mod n ============================
    LTC->MODE = LTC_PKHA_MOD_INV;
    LTC_Wait();
    // Read h from B
    uint8_t h[64];
    LTC_Read_Reg(&LTC->B[0], h, curve->n_size);
    // ================= 3. Calculate V = u*G ==================================
    // Set p size
    LTC->N_SIZE = curve->p_size;
    // Write p to N
    LTC_Write_Reg(&LTC->N[0], curve->p, curve->p_size);
    LTC->A_SIZE = curve->p_size;
    // Write a to A3
    LTC_Write_Reg(&LTC->A[16*3], curve->a, 64);
    // Write Gx to A0
    LTC_Write_Reg(&LTC->A[0], curve->Gx, 64);
    // Write Gy to A1
    LTC_Write_Reg(&LTC->A[16], curve->Gy, 64);
    // Write nonce to E
    LTC->E_SIZE = curve->n_size;
    LTC_Write_Reg(&LTC->E[0], nonce, curve->n_size);
    // Write b to B0
    LTC_Write_Reg(&LTC->B[0], curve->b, 64);
    // Perform multiplication, (B1, B2) = (Vx, Vy)
    LTC->MODE = LTC_PKHA_ECC_MUL_TEQ | curve->type;
    LTC_Wait();
    // ================= 4. Calculate c = Vx mod n =============================
    // Copy B1 to A0
    LTC->MODE = LTC_PKHA_COPY_SSIZE | LTC_PKHA_COPY_B1_A0;
    LTC_Wait();
    // Write n to N
    LTC->N_SIZE = curve->n_size;
    LTC_Write_Reg(&LTC->N[0], curve->n, curve->n_size);
    // Set mode to AmodN result to B
    LTC->MODE = LTC_PKHA_MOD_AMODN_B;
    LTC_Wait();
    // Read c from B
    LTC_Read_Reg(&LTC->B[0], sig->r, 64);
    // ================= 5. Calculate s*c mod n ================================
    // Write private key, s, to A0
    LTC->A_SIZE = curve->n_size;
    LTC_Write_Reg(&LTC->A[0], priv, curve->n_size);
    // Multiply with c
    LTC->MODE = LTC_PKHA_MOD_MUL;
    LTC_Wait();
    // ================= 6. Calculate f+s*c mod n ==============================
    // Write message representative f (e.g. hash) to A0
    LTC->A_SIZE = len;
    LTC_Write_Reg(&LTC->A[0], hash, len);
    // Add to previous result, B0 contains f+s*c mod n
    LTC->MODE = LTC_PKHA_MOD_ADD;
    LTC_Wait();
    // ================= 6. Calculate d = (h*(f+s*c)) mod n ====================
    // Write h to A0
    LTC->A_SIZE = curve->n_size;
    LTC_Write_Reg(&LTC->A[0], h, curve->n_size);
    // Multiply with f+s*c
    LTC->MODE = LTC_PKHA_MOD_MUL;
    LTC_Wait();
    // Return result
    LTC_Read_Reg(&LTC->B[0], sig->s, 64);
    return 0;
}

uint8_t CRYPTO_Bignum_Init(Bignum* num, uint8_t* buff, uint8_t len) {
    num->len = (len > CRYPTO_BIGNUM_SIZE) ? CRYPTO_BIGNUM_SIZE : len;
    for(uint8_t i=0; i<CRYPTO_BIGNUM_SIZE; i++)
        num->num[i] = (i < num->len) ? buff[i] : 0;
    num->len = CRYPTO_BIGNUM_SIZE;
    return 0;
}

uint8_t CRYPTO_Bignum_Sub(Bignum* min, Bignum* sub, Bignum* res) {
    // Clear all
    LTC->CW = LTC_CW_ALL;
    // Clear Done Interrupt
    LTC->STATUS |= LTC_STATUS_DI;
    // Set mode to PKHA and clear A,B,N,E memory
    LTC->MODE = LTC_PKHA_CLEARMEM_ABNE;
    LTC_Wait();
    // Write min to A
    LTC->A_SIZE = min->len;
    LTC_Write_Reg(&LTC->A[0], min->num, min->len);
    // Write sub to A
    LTC->B_SIZE = sub->len;
    LTC_Write_Reg(&LTC->B[0], sub->num, sub->len);
    // Write mod to N
    LTC->N_SIZE = secp256k1.p_size;
    LTC_Write_Reg(&LTC->N[0], secp256k1.p, secp256k1.p_size);
    // Perform modulo operation
    LTC->MODE = LTC_PKHA_MOD_SUB1;
    LTC_Wait();
    // Read result from B
    LTC_Read_Reg(&LTC->B[0], res->num, res->len);
    return 0;
}

uint8_t CRYPTO_Bignum_Mod(Bignum* num, Bignum* mod, Bignum* res) {
    // Clear all
    LTC->CW = LTC_CW_ALL;
    // Clear Done Interrupt
    LTC->STATUS |= LTC_STATUS_DI;
    // Set mode to PKHA and clear A,B,N,E memory
    LTC->MODE = LTC_PKHA_CLEARMEM_ABNE;
    LTC_Wait();
    // Write num to A
    LTC->A_SIZE = num->len;
    LTC_Write_Reg(&LTC->A[0], num->num, num->len);
    // Write mod to N
    LTC->N_SIZE = mod->len;
    LTC_Write_Reg(&LTC->N[0], mod->num, mod->len);
    // Perform modulo operation
    LTC->MODE = LTC_PKHA_MOD_AMODN_B;
    LTC_Wait();
    // Read result from B
    LTC_Read_Reg(&LTC->B[0], res->num, res->len);
    return 0;
}

uint8_t CRYPTO_Bignum_Div(Bignum* num, Bignum* divisor, Bignum* res) {
    Bignum tmp;
    tmp.len = CRYPTO_BIGNUM_SIZE;
    CRYPTO_Bignum_Mod(num, divisor, &tmp);
    CRYPTO_Bignum_Sub(num, &tmp, &tmp);
    // Write div to A
    for(uint8_t i=0; i<CRYPTO_BIGNUM_SIZE; i++)
        LTC->A[i] = 0;
    LTC->A_SIZE = divisor->len;
    LTC_Write_Reg(&LTC->A[0], divisor->num, divisor->len);
    // Write mod to N
    LTC->N_SIZE = secp256k1.p_size;
    LTC_Write_Reg(&LTC->N[0], secp256k1.p, secp256k1.p_size);
    // Perform inversion operation
    LTC->MODE = LTC_PKHA_MOD_INV;
    LTC_Wait();
    // Write num to A
    LTC->A_SIZE = CRYPTO_BIGNUM_SIZE;
    LTC_Write_Reg(&LTC->A[0], tmp.num, tmp.len);
    // Perform multiplication operation
    LTC->MODE = LTC_PKHA_MOD_MUL;
    LTC_Wait();
    // Read result from B
    LTC_Read_Reg(&LTC->B[0], res->num, res->len);
    return 0;
}

uint8_t CRYPTO_Bignum_IsNull(Bignum* num) {
    if(!num) return 1;

    for(uint8_t i=0; i < num->len; i++) {
        if(num->num[i])
            return 0;
    }
    return 1;
}

#ifdef KL82

static void LTC_MDHA_Operation(uint32_t op, uint8_t* buff, uint32_t len, uint8_t* result) {
    LTC->MODE = op;
    LTC->DS = len;
    if(op == LTC_MDHA_SHA256_UPDATE || op == LTC_MDHA_SHA256_FINALIZE) {
        LTC_Write_Reg(&LTC->CTX[0], result, 64);
    }
    for(uint32_t i=0; i<len; i+=4) {
        uint32_t status = LTC->FIFOSTATUS;
        while(status & LTC_FIFOSTATUS_IFF) {
            status = LTC->FIFOSTATUS;
        }
        // Check if FIFO is full
        uint32_t tmp = 0;
        for(uint8_t j=0; j<=3; j++) {
            tmp = (tmp<<8) | buff[i+j];
        }
        LTC->IFIFO = tmp;
    }
    LTC_Wait();
    LTC_Read_Reg(&LTC->CTX[0], result, 64);
    LTC->CW = LTC_CW_ALL;
}

uint8_t CRYPTO_SHA256(uint8_t* buff, uint32_t len, uint32_t* result) {
    uint8_t tmpres[64];
    LTC->CW = LTC_CW_ALL;
    if(len <= 64) {
        LTC_MDHA_Operation(LTC_MDHA_SHA256_INITFIN, buff, len, tmpres);
    } else {
        LTC_MDHA_Operation(LTC_MDHA_SHA256_INIT, buff, 64, tmpres);
        buff += 64;
        len -= 64;
        if(len > 64) {
            uint32_t left = len%64;
            LTC_MDHA_Operation(LTC_MDHA_SHA256_UPDATE, buff, len-left, tmpres);
            buff += (len-left);
            len = left;
        }
        LTC_MDHA_Operation(LTC_MDHA_SHA256_FINALIZE, buff, len, tmpres);
    }
    for(uint8_t i=0; i<8; i++)
        result[7-i] = *(uint32_t*)(tmpres+i*4);
    return 0;
}

#else

static const uint32_t sha256_k[64] = {
    0x428A2F98u, 0x71374491u, 0xB5C0FBCFu, 0xE9B5DBA5u,
    0x3956C25Bu, 0x59F111F1u, 0x923F82A4u, 0xAB1C5ED5u,
    0xD807AA98u, 0x12835B01u, 0x243185BEu, 0x550C7DC3u,
    0x72BE5D74u, 0x80DEB1FEu, 0x9BDC06A7u, 0xC19BF174u,
    0xE49B69C1u, 0xEFBE4786u, 0x0FC19DC6u, 0x240CA1CCu,
    0x2DE92C6Fu, 0x4A7484AAu, 0x5CB0A9DCu, 0x76F988DAu,
    0x983E5152u, 0xA831C66Du, 0xB00327C8u, 0xBF597FC7u,
    0xC6E00BF3u, 0xD5A79147u, 0x06CA6351u, 0x14292967u,
    0x27B70A85u, 0x2E1B2138u, 0x4D2C6DFCu, 0x53380D13u,
    0x650A7354u, 0x766A0ABBu, 0x81C2C92Eu, 0x92722C85u,
    0xA2BFE8A1u, 0xA81A664Bu, 0xC24B8B70u, 0xC76C51A3u,
    0xD192E819u, 0xD6990624u, 0xF40E3585u, 0x106AA070u,
    0x19A4C116u, 0x1E376C08u, 0x2748774Cu, 0x34B0BCB5u,
    0x391C0CB3u, 0x4ED8AA4Au, 0x5B9CCA4Fu, 0x682E6FF3u,
    0x748F82EEu, 0x78A5636Fu, 0x84C87814u, 0x8CC70208u,
    0x90BEFFFAu, 0xA4506CEBu, 0xBEF9A3F7u, 0xC67178F2u
};

uint8_t CRYPTO_SHA256(uint8_t* buff, uint32_t len, uint32_t* result) {
    if(!buff || !result || len == 0) {
        return 1;
    }

    uint32_t num_blocks = (len + 9)/64;
    if((len + 9) % 64){
        num_blocks++;
    }

    MMCAU_LDR->CA[0] = result[0] = 0x6A09E667u;
    MMCAU_LDR->CA[1] = result[1] = 0xBB67AE85u;
    MMCAU_LDR->CA[2] = result[2] = 0x3C6EF372u;
    MMCAU_LDR->CA[3] = result[3] = 0xA54FF53Au;
    MMCAU_LDR->CA[4] = result[4] = 0x510E527Fu;
    MMCAU_LDR->CA[5] = result[5] = 0x9B05688Cu;
    MMCAU_LDR->CA[6] = result[6] = 0x1F83D9ABu;
    MMCAU_LDR->CA[7] = result[7] = 0x5BE0CD19u;

    uint32_t W[64];
    uint32_t count = 0;
    uint64_t bit_length = len*8;
    while(num_blocks--) {
        int i;
        for(i=0; i<16; i++) {
            uint32_t tmp = 0;
            for(int j=3; j>=0 && count <= len; j--, count++) {
                if(count < len)
                    tmp |= buff[count] << (8*j);
                else if(count == len)
                    tmp |= 0x80 << (8*j);
            }
            if(num_blocks == 0){
                if(i == 14)
                    tmp = bit_length >> 32;
                else if(i == 15)
                    tmp = bit_length & 0xFFFFFFFFu;
            }

            W[i] = tmp;
            MMCAU_LDR->CAA = W[i];
            MMCAU->DIRECT = MMCAU_CMD_SIGMA1_CH;
            MMCAU_ADR->CAA = sha256_k[i];
            MMCAU->DIRECT = MMCAU_CMD_SIGMA0_MAJ;
            MMCAU->DIRECT = MMCAU_CMD_SHIFT;
        }

        for(; i<64; i++) {
            MMCAU_LDR->CAA      = W[i-16];
            MMCAU_LDR->CA[8]    = W[i-15];
            MMCAU->DIRECT       = MMCAU_CMD_SIGMA0;
            MMCAU_ADR->CAA      = W[i-7];
            MMCAU_LDR->CA[8]    = W[i-2];
            MMCAU->DIRECT       = MMCAU_CMD_SIGMA1;
            W[i]                = MMCAU_STR->CAA;
            MMCAU->DIRECT       = MMCAU_CMD_SIGMA1_CH;
            MMCAU_ADR->CAA      = sha256_k[i];
            MMCAU->DIRECT       = MMCAU_CMD_SIGMA0_MAJ;
            MMCAU->DIRECT       = MMCAU_CMD_SHIFT;
        }

        for(int i=0; i<8; i++) {
            MMCAU_ADR->CA[i] = result[i];
        }
        for(int i=0; i<8; i++) {
            result[i] = MMCAU_STR->CA[i];
        }
    }

    for(int i=0; i<8; i++) {
        result[7-i] = MMCAU_STR->CA[i];
    }

    return 0;
}

#endif

void CRYPTO_Init() {
    CLOCK_Enable(CLOCK_LTC);
    CLOCK_Enable(CLOCK_TRNG);
    // Init TRNG
    // PGM mode, reset to defaults, clear ERR and use von Neumann sample mode
    TRNG->MCTL = 0x11040u;
    TRNG->MCTL &= ~(0x3u);
    #ifdef KL82
    TRNG->MCTL |= 0x8;
    #endif
    // Leave PGM mode and lock register access
    TRNG->MCTL = (TRNG->MCTL & ~0x10000u)| 0x20;
    TRNG->SECCFG |= 2u;
}

uint8_t CRYPTO_Random(uint8_t* buff, uint32_t len) {
    uint32_t status;
    // Flush entropy
    for(int i=0; i<16; i++)
        status = TRNG->ENT[i];
    for(uint32_t i=0, ent=0; i<len; i+=4, ent++) {
        status = 0;
        while(!(status & (TRNG_MCTL_ERR | TRNG_MCTL_ENTVAL))) {
            status = TRNG->MCTL;
        }
        if(status & TRNG_MCTL_ERR) {
            TRNG->MCTL |= TRNG_MCTL_ERR;
            return 1;
        }

        uint32_t random = TRNG->ENT[ent%16];
        for(uint32_t j=0; j<4; j++) {
            if((i+j) == len)
                return 0;
            buff[i+j] = (random >> (8*j)) & 0xFF;
        }
    }
    return 0;
}
