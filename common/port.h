//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#ifndef _PORT_H_
#define _PORT_H_

#include <stdint.h>

typedef struct {
    volatile uint32_t PCR[32];
    volatile uint32_t GPCLR;
    volatile uint32_t GPCHR;
    uint8_t  RESERVED_0[24];
    volatile uint32_t ISFR;
    uint8_t  RESERVED_1[28];
    volatile uint32_t DFER;
    volatile uint32_t DFCR;
    volatile uint32_t DFWR;
} PORTx;

#define PORT_A      ((PORTx*) 0x40049000u)
#define PORT_B      ((PORTx*) 0x4004A000u)
#define PORT_C      ((PORTx*) 0x4004B000u)
#define PORT_D      ((PORTx*) 0x4004C000u)
#define PORT_E      ((PORTx*) 0x4004D000u)

static inline void PORT_PinMux(PORTx* port, uint8_t pin, uint8_t mux) {
    port->PCR[pin] = (port->PCR[pin] & ~(0x700u)) | ((mux << 8) & 0x700u);
}

#endif
