//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#ifndef _CLOCK_H_
#define _CLOCK_H_

#include <stdint.h>

typedef struct {
    volatile uint32_t SOPT1;
    volatile uint32_t SOPT1CFG;
    uint8_t RESERVED_0[4092];
    volatile uint32_t SOPT2;
    uint8_t RESERVED_1[4];
    volatile uint32_t SOPT4;
    volatile uint32_t SOPT5;
    uint8_t RESERVED_2[4];
    volatile uint32_t SOPT7;
    volatile uint32_t SOPT8;
    volatile uint32_t SOPT9;
    volatile uint32_t SDID;
    volatile uint32_t SCGC[7];
    volatile uint32_t CLKDIV1;
    volatile uint32_t CLKDIV2;
    volatile uint32_t FCFG1;
    volatile uint32_t FCFG2;
    volatile uint32_t UIDH;
    volatile uint32_t UIDMH;
    volatile uint32_t UIDML;
    volatile uint32_t UIDL;
    volatile uint32_t CLKDIV3;
    volatile uint32_t CLKDIV4;
} _SIM;

#define SIM         ((_SIM*)0x40047000u)

typedef struct {
    volatile uint8_t C1;
    volatile uint8_t C2;
    volatile uint8_t C3;
    volatile uint8_t C4;
    volatile uint8_t C5;
    volatile uint8_t C6;
    volatile uint8_t S;
    uint8_t RESERVED_0[1];
    volatile uint8_t SC;
    uint8_t RESERVED_1[1];
    volatile uint8_t ATCVH;
    volatile uint8_t ATCVL;
    volatile uint8_t C7;
    volatile uint8_t C8;
} _MCG;

#define MCG         ((_MCG*)0x40064000u)

typedef struct {
    volatile uint8_t CR;
    uint8_t RESERVED_0[1];
    volatile uint8_t DIV;
} _OSC;

#define OSC         ((_OSC*)0x40065000u)

#ifdef KL82
#define CLOCK_UART0     (4<<8 | 20)
#define CLOCK_UART1     (4<<8 | 21)
#define CLOCK_UART2     (4<<8 | 22)
#else
#define CLOCK_UART0     (1<<8 | 4)
#define CLOCK_UART1     (1<<8 | 5)
#define CLOCK_UART2     (1<<8 | 6)
#define CLOCK_UART3     (1<<8 | 7)
#define CLOCK_UART4     (1<<8 | 22)
#endif

#define CLOCK_SPI0      (5<<8 | 12)
#define CLOCK_SPI1      (5<<8 | 13)
#ifndef KL82
#define CLOCK_SPI2      (2<<8 | 12)
#endif

#define CLOCK_I2C0      (3<<8 | 6)
#define CLOCK_I2C1      (3<<8 | 7)

#define CLOCK_PORTA     (4<<8 | 9)
#define CLOCK_PORTB     (4<<8 | 10)
#define CLOCK_PORTC     (4<<8 | 11)
#define CLOCK_PORTD     (4<<8 | 12)
#define CLOCK_PORTE     (4<<8 | 13)

#ifdef KL82
#define CLOCK_LTC       (4<<8 | 17)
#define CLOCK_TRNG      (5<<8 | 5)
#else
#define CLOCK_LTC       (1<<8 | 17)
#define CLOCK_TRNG      (2<<8 | 0)
#endif
#define CLOCK_CRC       (5<<8 | 18)

static inline void CLOCK_Enable(uint16_t regbit) {
    SIM->SCGC[(regbit>>8)&0x7] |= 1<<(regbit&0x1Fu);
}

static inline void CLOCK_SourceUART(uint8_t src) {
    SIM->SOPT2 = ((SIM->SOPT2) & ~(0xC000000u)) | ((src & 0x3u) << 26u);
}

void CLOCK_Init(void);

#endif
