//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#include "spi.h"
#include "clock.h"

uint8_t SPI_Init(SPIx* spi, uint8_t mode) {
    if(spi == SPI_0) {
        CLOCK_Enable(CLOCK_SPI0);
    } else if(spi == SPI_1) {
        CLOCK_Enable(CLOCK_SPI1);
    }
    #ifndef KL82
    else if(spi == SPI_2) {
        CLOCK_Enable(CLOCK_SPI2);
    }
    #endif
    else return 1;

    // Master mode + HALT + CS0 active low
    spi->MCR = 0x80010001u;
    // Set baud rate
    uint8_t prescaler, scaler, dbr;
    switch(mode & 0xFCu) {
        case SPI_MODE_500KHZ:
            prescaler = 0;
            scaler = 6;
            dbr = 0;
            break;
        case SPI_MODE_1MHZ:
            prescaler = 0;
            scaler = 5;
            dbr = 0;
            break;
        default:
            return 1;
    }

    // 8-bit transfer
    spi->CTAR[0] = 0x38000000u | (dbr<<31) | (prescaler<<22) |
                   (prescaler<<20) | (prescaler<<18) | (prescaler<<16) |
                   (scaler<<12) | (scaler<<8) | (scaler<<4) | scaler |
                   ((mode & 0x3u) << 25);

    // Clean HALT
    spi->MCR &= ~(0x00000001u);
    return 0;
}

uint8_t SPI_WriteAsync(SPIx* spi, uint8_t* buff, uint32_t len) {
    spi->MCR |= 1;
    spi->MCR |= 0x00000C00u;
    spi->SR |= spi->SR;
    spi->MCR &= ~(0x00000001u);
    while(len--) {
        // Tx FIFO fill req?
        while(!(spi->SR & 0x2000000u));

        if(len == 0) {
            spi->PUSHR = 0x8010000u | *(buff++);
        } else {
            spi->PUSHR = 0x80010000u | *(buff++);
        }
        spi->SR = 0x82000000u;
        // Tx complete?
        while(!(spi->SR & 0x80000000u));
    }
    return 0;
}
