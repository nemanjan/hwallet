//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#ifndef _FLASH_H_
#define _FLASH_H_
#include <stdint.h>

typedef struct {
    volatile uint8_t FSTAT;
    volatile uint8_t FCNFG;
    volatile uint8_t FSEC;
    volatile uint8_t FOPT;
    volatile uint32_t FCCOB[3];
    volatile uint32_t FPROT;
    uint8_t RESERVED_0[4];
    volatile uint32_t XACCH;
    volatile uint32_t XACCL;
    volatile uint32_t SACCH;
    volatile uint32_t SACCL;
    volatile uint8_t FACSS;
    uint8_t RESERVED_1[2];
    volatile uint8_t FACSN;
} _FTFA;

#define FTFA    ((_FTFA*) 0x40020000u)
#define FTFA_FSTAT_CCIF     (1<<7)
#define FTFA_FSTAT_ACCERR   (1<<5)
#define FTFA_FSTAT_FPVIOL   (1<<4)

#define FLASH_SECTOR_SIZE       0x1000u
#ifdef KL82
#define FLASH_TOP               0x20000u
#else
#define FLASH_TOP               0x40000u
#endif
#define FLASH_BITCOIN_PRIVKEY   (FLASH_TOP - FLASH_SECTOR_SIZE)

uint8_t FLASH_Write(uint32_t* src, uint32_t len, uint32_t dst);
uint8_t FLASH_SectorErase(uint32_t sec);

#endif
