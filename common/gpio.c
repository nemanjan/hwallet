//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#include "gpio.h"

void GPIO_PinInit(GPIOx* gpio, uint8_t pin) {
    if(pin & (1 << 7)) {
        // Direction: IN
        gpio->PDDR &= ~(1 << (pin & 0x1F));
    } else {
        // Direction: OUT
        GPIO_WriteOutput(gpio, (pin & 0x1F), ((pin >> 6) & 1));
        gpio->PDDR |= (1 << (pin & 0x1F));
    }
}

void GPIO_WriteOutput(GPIOx* gpio, uint8_t pin, uint8_t value) {
    if(value) {
        gpio->PSOR = 1 << pin;
    } else {
        gpio->PCOR = 1 << pin;
    }
}

uint8_t GPIO_ReadInput(GPIOx* gpio, uint8_t pin) {
    return (gpio->PDIR & (1 << pin));
}
