//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#include "clock.h"

#ifdef KL82
/*
xtal osc freq = 12 MHz
PRDIV0 = 0+1
VDIV = 8+16
CPU clock = 72 MHz
Bus clock = 120/2 = 60 MHz
*/

void CLOCK_Init(void) {
    // Set the divider to default values
    SIM->CLKDIV1 = 0x15051000u;
    // Enable external reference clock and set divider to 1
    OSC->CR = 1<<7u;
    OSC->DIV = 0;
    // Use very high frequency oscillator
    MCG->C2 |= (2<<4u) | (1<<2u);
    // Wait for it to stabilize
    while(!(MCG->S & 0x2u));

    // Configure internal reference clock
    // FCRDIV = 0 -> divider is 1
    MCG->SC = 0;
    // MCGIRCLK active
    MCG->C1 |= 0x2u;

    // Enter PBE mode for MCG
    // Select external clock
    MCG->C1 = (MCG->C1 & ~(0xC4u)) | 0x80u;
    while((MCG->S & 0x1Cu) != 0x8u);
    // Disable PLL
    MCG->C6 &= ~(0x40u);
    while(MCG->S & 0x20u);
    // Configure PLL
    // Set VDIV
    MCG->C6 = (MCG->C6 & 0x1Fu) | 0x8u;
    // MCGPLLCLK enable
    MCG->C5 = 0x40u;
    while(!(MCG->S & 0x40u));
    // Enable PLL
    MCG->C6 |= 0x40u;
    while(!(MCG->S & 0x20u));

    // Switch to PLL as a clock source
    MCG->C1 &= ~(0xC0u);
    while((MCG->S & 0xCu) != 0xCu);

    // Set SIM regs
    // PLLFLLSEL = 1 -> MCGPLLCLK clock
    SIM->SOPT2 |= 1 << 0x10u;
    // OSC32KSEL = 2 -> RTC 32k oscillator
    SIM->SOPT1 = (SIM->SOPT1 & ~(3<<18)) | (2<<18);
}

#else
/*
xtal osc freq = 12 MHz
PRDIV0 = 0+1
VDIV = 4+16
CPU clock = 120 MHz
Bus clock = 120/2 = 60 MHz
*/

void CLOCK_Init(void) {
    // Set the divider to default values
    SIM->CLKDIV1 = 0x01140000u;
    // Enable external reference clock and set divider to 1
    OSC->CR = 1<<7u;
    OSC->DIV = 0;
    // Use very high frequency oscillator
    MCG->C2 |= (2<<4u) | (1<<2u);
    // Wait for it to stabilize
    while(!(MCG->S & 0x2u));

    // Configure internal reference clock
    // FCRDIV = 0 -> divider is 1
    MCG->SC = 0;
    // MCGIRCLK active
    MCG->C1 |= 0x2u;

    // Enter PBE mode for MCG
    // Select external clock
    MCG->C1 = (MCG->C1 & ~(0xC4u)) | 0x80u;
    while((MCG->S & 0x1Cu) != 0x8u);
    // Disable PLL
    MCG->C6 &= ~(0x40u);
    while(MCG->S & 0x20u);
    // Configure PLL
    // Set VDIV
    MCG->C6 = (MCG->C6 & 0x1Fu) | 0x4u;
    // MCGPLLCLK enable
    MCG->C5 = 0x40u;
    while(!(MCG->S & 0x40u));
    // Enable PLL
    MCG->C6 |= 0x40u;
    while(!(MCG->S & 0x20u));

    // Switch to PLL as a clock source
    MCG->C1 &= ~(0xC0u);
    while((MCG->S & 0xCu) != 0xCu);

    // Set SIM regs
    // PLLFLLSEL = 1 -> MCGPLLCLK clock
    SIM->SOPT2 |= 1 << 0x10u;
    // OSC32KSEL = 2 -> RTC 32k oscillator
    SIM->SOPT1 = (SIM->SOPT1 & ~(3<<18)) | (2<<18);

}
#endif
