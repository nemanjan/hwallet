//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#ifndef _BITCOIN_H_
#define _BITCOIN_H_
#include <stdint.h>
#include "common/gpio.h"
#include "common/packet.h"

#define BITCOIN_FUNC_KEYPAIR_GET        0u
#define BITCOIN_FUNC_KEYPAIR_SET        1u
#define BITCOIN_FUNC_KEYPAIR_GENERATE   2u
#define BITCOIN_FUNC_INIT_TX            3u
#define BITCOIN_FUNC_ADD_INPUT          4u
#define BITCOIN_FUNC_ADD_OUTPUT         5u
#define BITCOIN_FUNC_SIGN_TX            6u

#define BITCOIN_MAX_INPUTS      10
#define BITCOIN_MAX_OUTPUTS     10

#define BITCOIN_TX_BUFF_SIZE     1024u

#define BITCOIN_MAX_SCRIPT_LEN   250u

typedef struct {
    uint8_t hash[32];
    uint32_t index;
    uint8_t scriptLen;
    uint8_t script[BITCOIN_MAX_SCRIPT_LEN];
} Bitcoin_Tx_Input;

typedef struct {
    uint64_t value;
    uint32_t scriptLen;
    uint8_t script[BITCOIN_MAX_SCRIPT_LEN];
} Bitcoin_Tx_Output;

#define BITCOIN_OP_DUP          0x76u
#define BITCOIN_OP_HASH160      0xA9u
#define BITCOIN_OP_EQUALVERIFY  0x88u
#define BITCOIN_OP_CHECKSIG     0xACu

void Bitcoin_Init(GPIOx* yesgpio, uint8_t yespin, GPIOx* nogpio, uint8_t nopin);
void Bitcoin_Process(Packet* msg);

#endif
