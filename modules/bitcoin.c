//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#include "bitcoin.h"
#include "common/utils.h"
#include "common/crypto.h"
#include "common/gpio.h"
#include "common/oled.h"
#include "common/flash.h"

static uint8_t privateKey[32];
static uint8_t publicKey[64];

static uint8_t txbuff[BITCOIN_TX_BUFF_SIZE];
static uint32_t txlen = 0;

static uint8_t inputNum = 0, outputNum = 0;
static Bitcoin_Tx_Input inputs[BITCOIN_MAX_INPUTS];
static Bitcoin_Tx_Output outputs[BITCOIN_MAX_OUTPUTS];

static GPIOx* yesGpio;
static GPIOx* noGpio;
static uint8_t yesPin, noPin;

void Bitcoin_Init(GPIOx* yesgpio, uint8_t yespin, GPIOx* nogpio, uint8_t nopin) {
    yesGpio = yesgpio;
    noGpio = nogpio;
    yesPin = yespin;
    noPin = nopin;
    uint8_t *src = (uint8_t*)FLASH_BITCOIN_PRIVKEY;
    for(uint8_t i=0; i<32; i++) {
        privateKey[i] = src[i];
    }
    CRYPTO_ECDSA_GetPublicKey(&secp256k1, privateKey, publicKey);
}

void Bitcoin_Keypair_Generate(void) {
    CRYPTO_Random(privateKey, 32);
    CRYPTO_ECDSA_GetPublicKey(&secp256k1, privateKey, publicKey);
    FLASH_Write((uint32_t*)privateKey, 32, FLASH_BITCOIN_PRIVKEY);
    PACKET_SendACK();
}

void Bitcoin_Keypair_Get(void) {
    uint8_t *src = (uint8_t*)FLASH_BITCOIN_PRIVKEY;
    for(uint8_t i=0; i<32; i++) {
        privateKey[i] = src[i];
    }
    PACKET_Send(PACKET_RSP_PRIVKEY, privateKey, 32);
    PACKET_Send(PACKET_RSP_PUBKEY, publicKey, 64);
}

void Bitcoin_Keypair_Set(Packet* msg) {
    for(uint8_t i=0; i<32; i++) {
        privateKey[i] = msg->data[i];
    }
    CRYPTO_ECDSA_GetPublicKey(&secp256k1, privateKey, publicKey);
    FLASH_Write((uint32_t*)privateKey, 32, FLASH_BITCOIN_PRIVKEY);
    PACKET_SendACK();
}

void Bitcoin_Tx_Init(void) {
    inputNum = outputNum = txlen = 0;
    PACKET_SendACK();
}

void Bitcoin_Add_Input(void) {
    if(inputNum >= BITCOIN_MAX_INPUTS) {
        PACKET_SendNAK();
        return;
    }

    uint8_t* buff;
    uint32_t len;
    buff = PACKET_GetLong(&len);

    for(uint8_t i=0; i<32; i++)
        inputs[inputNum].hash[i] = buff[i];

    inputs[inputNum].index = *((uint32_t*)(buff+32));

    inputNum++;
    PACKET_SendACK();
}

void Bitcoin_Add_Output(void) {
    if(outputNum >= BITCOIN_MAX_OUTPUTS) {
        PACKET_SendNAK();
        return;
    }

    uint8_t* buff;
    uint32_t len;
    buff = PACKET_GetLong(&len);

    outputs[outputNum].value = *((uint64_t*)buff);
    uint8_t scriptlen = buff[8];
    outputs[outputNum].scriptLen = scriptlen;

    for(uint8_t i=0; i<scriptlen; i++)
        outputs[outputNum].script[i] = buff[9+i];

    outputNum++;
    PACKET_SendACK();
}

void Bitcoin_Output_Value(char* str, uint8_t output) {
    char strbtc[10], strsat[10];
    Bignum val, btc, sat, btcsat;
    uint32_t btc2sat = 100000000;

    CRYPTO_Bignum_Init(&val, (uint8_t*)&outputs[output].value, 8);
    CRYPTO_Bignum_Init(&btcsat, (uint8_t*)&btc2sat, 4);
    btcsat.len = 4;
    CRYPTO_Bignum_Init(&btc, (uint8_t*)&btc2sat, 4);
    CRYPTO_Bignum_Init(&sat, (uint8_t*)&btc2sat, 4);
    CRYPTO_Bignum_Mod(&val, &btcsat, &sat);
    CRYPTO_Bignum_Div(&val, &btcsat, &btc);

    UTIL_IntToDec(btc.num, 8, strbtc, sizeof(strbtc));
    UTIL_IntToDec(sat.num, 8, strsat, sizeof(strsat));
    uint8_t strpos = 0;
    for(uint8_t i=0; i<sizeof(strbtc)-1; i++) {
        if(strbtc[i] != ' ') {
            str[strpos++] = strbtc[i];
        }
    }
    str[strpos++] = '.';
    for(uint8_t i=1; i<sizeof(strsat)-1; i++) {
        if(strsat[i] != ' ') {
            str[strpos++] = strsat[i];
        } else {
            str[strpos++] = '0';
        }
    }
    str[strpos++] = ' ';
    str[strpos++] = 'B'; str[strpos++] = 'T'; str[strpos++] = 'C';
    str[strpos] = 0;
}

void Bitcoin_Output_Address(char* str, uint8_t output) {
    Bitcoin_Tx_Output *out = &outputs[output];
    str[0] = 0;
    // Analyze desired output
    // P2PKH
    if(out->scriptLen != 0x19) {
        return;
    }
    if(out->script[0] != BITCOIN_OP_DUP ||
       out->script[1] != BITCOIN_OP_HASH160 ||
       out->script[2] != 0x14 ||
       out->script[0x17] != BITCOIN_OP_EQUALVERIFY ||
       out->script[0x18] != BITCOIN_OP_CHECKSIG) {
           return;
    }
    uint8_t addr[25];
    addr[24] = 0;
    for(int i=0;i<25;i++) {
        addr[23-i] = out->script[3+i];
    }
    uint8_t tohash[21];
    tohash[0] = 0;
    for(int i=1;i<21;i++) {
        tohash[i] = addr[24-i];
    }
    uint32_t result[8], result1[8];
    CRYPTO_SHA256(tohash, 21, result1);
    for(int i=0;i<4;i++){
        uint32_t tmp = __builtin_bswap32(result1[7-i]);
        result1[7-i]=__builtin_bswap32(result1[i]);
        result1[i] = tmp;
    }
    CRYPTO_SHA256((uint8_t*)result1, 32, result);
    for(int i=0; i<4; i++) {
        addr[i] = (result[7]>>(8*i)) & 0xFF;
    }
    UTIL_IntToBase58(addr, sizeof(addr), str, 40);
}

void Bitcoin_Prepare_Tx(uint8_t input) {
    txlen = 0;
    // Version = 1
    txbuff[txlen++] = 1;
    for(uint8_t j=0; j<3; j++)
        txbuff[txlen++] = 0;
    // Add inputs
    txbuff[txlen++] = inputNum;
    for(uint8_t i=0; i<inputNum; i++) {
        for(int8_t j=31; j>=0; j--)
            txbuff[txlen++] = inputs[i].hash[j];
        for(uint8_t j=0; j<4; j++)
            txbuff[txlen++] = (inputs[i].index >> (8*j)) & 0xFFu;
        if(input != 0xFFu) {
            if(i == input) {
                txbuff[txlen++] = 0x19;
                for(uint8_t j=0; j<0x19; j++)
                    txbuff[txlen++] = outputs[outputNum-1].script[j];
            } else {
                txbuff[txlen++] = 0;
            }
        } else {
            txbuff[txlen++] = inputs[i].scriptLen;
            for(uint8_t j=0; j<inputs[i].scriptLen; j++)
                txbuff[txlen++] = inputs[i].script[j];
        }
        for(uint8_t j=0; j<4; j++)
            txbuff[txlen++] = 0xFFu;
    }
    // Add outputs
    txbuff[txlen++] = outputNum;
    for(uint8_t i=0; i<outputNum; i++) {
        for(uint8_t j=0;j<8;j++)
            txbuff[txlen++]=(outputs[i].value >> j*8)&0xFFu;
        txbuff[txlen++] = outputs[i].scriptLen;
        for(uint8_t j=0; j<outputs[i].scriptLen; j++)
            txbuff[txlen++] = outputs[i].script[j];
    }
    // Locktime
    for(uint8_t j=0; j<4; j++)
        txbuff[txlen++] = 0;
    // SIGHASH
    if(input != 0xFFu) {
        txbuff[txlen++] = 1;
        for(uint8_t j=0; j<3; j++)
            txbuff[txlen++] = 0;
    }
}

void Bitcoin_Write_Signature(uint8_t input, ECDSA_Signature* sig) {
    uint8_t i=0;
    // BIP-66
    uint8_t rlen = (sig->r[31] & 0x80) ? 0x21 : 0x20;
    uint8_t slen = 0x20;
    Bignum s;
    CRYPTO_Bignum_Init(&s, sig->s, 32);
    if(sig->s[31] & 0x80) {
        // BIP-62
        Bignum n;
        CRYPTO_Bignum_Init(&n, (uint8_t*)secp256k1.n, 32);
        CRYPTO_Bignum_Sub(&n, &s, &s);
    }
    inputs[input].script[i++]=rlen+slen+7;
    inputs[input].script[i++]=0x30;
    inputs[input].script[i++]=rlen+slen+4;
    inputs[input].script[i++]=0x02;
    inputs[input].script[i++]=rlen;
    if(rlen == 0x21)
        inputs[input].script[i++]=0x00;
    for(int8_t j=32; j>0; j--)
        inputs[input].script[i++]=sig->r[j-1];
    inputs[input].script[i++]=0x02;
    inputs[input].script[i++]=slen;
    for(int8_t j=32; j>0; j--)
        inputs[input].script[i++]=s.num[j-1];
    inputs[input].script[i++]=0x01;
    inputs[input].script[i++]=0x41;
    inputs[input].script[i++]=0x04;
    for(uint8_t j=0; j<64; j++)
        inputs[input].script[i++]=publicKey[63-j];
    inputs[input].scriptLen = i;
}

void Bitcoin_Tx_Sign(void) {
    // Approve all tx outputs
    for(uint8_t i=0; i<outputNum; i++) {
        char strval[30], straddr[40];
        Bitcoin_Output_Value(strval, i);
        Bitcoin_Output_Address(straddr, i);
        straddr[0] = 't'; straddr[1] = 'o';
        OLED_Clear();
        OLED_WriteRow(0, strval);
        char tmp = straddr[20];
        straddr[20] = 0;
        OLED_WriteRow(10, straddr);
        straddr[20] = tmp;
        OLED_WriteRow(20, straddr+20);

        while(GPIO_ReadInput(yesGpio, yesPin) && GPIO_ReadInput(noGpio, noPin));
        if(GPIO_ReadInput(yesGpio, yesPin) == 0) {
            OLED_Clear();
            OLED_WriteRow(10, "    CONFIRMED");
            for(uint32_t j=0; j<7000000; j++);
        } else {
            PACKET_SendNAK();
            OLED_Clear();
            OLED_WriteRow(10, "     REJECTED");
            for(uint32_t j=0; j<7000000; j++);
            OLED_Clear();
            OLED_ShowHomeScreen();
            return;
        }
    }
    // Sign tx for each input
    for(uint8_t i=0; i<inputNum; i++) {
        Bitcoin_Prepare_Tx(i);
        ECDSA_Signature sig;
        uint32_t result[8], result1[8];
        CRYPTO_SHA256(txbuff, txlen, result1);
        for(int i=0;i<4;i++){
            uint32_t tmp = __builtin_bswap32(result1[7-i]);
            result1[7-i]=__builtin_bswap32(result1[i]);
            result1[i] = tmp;
        }
        CRYPTO_SHA256((uint8_t*)result1, 32, result);
        CRYPTO_ECDSA_Sign(&secp256k1, privateKey, (uint8_t*)result,
                          sizeof(result), &sig);

        Bitcoin_Write_Signature(i, &sig);
    }
    // Assemble final tx
    Bitcoin_Prepare_Tx(0xFFu);
    // Return signed tx
    PACKET_Send(PACKET_RSP_TX, txbuff, txlen);
    for(uint32_t j=0; j<9999999; j++);
    OLED_Clear();
    OLED_ShowHomeScreen();
}

void Bitcoin_Process(Packet* msg) {
    switch(PACKET_FUNC(msg->type)) {
        case BITCOIN_FUNC_KEYPAIR_GET:
            Bitcoin_Keypair_Get();
            break;
        case BITCOIN_FUNC_KEYPAIR_SET:
            Bitcoin_Keypair_Set(msg);
            break;
        case BITCOIN_FUNC_KEYPAIR_GENERATE:
            Bitcoin_Keypair_Generate();
            break;
        case BITCOIN_FUNC_INIT_TX:
            Bitcoin_Tx_Init();
            break;
        case BITCOIN_FUNC_ADD_INPUT:
            if(PACKET_Long(msg) == PACKET_LONG_DONE)
                Bitcoin_Add_Input();
            break;
        case BITCOIN_FUNC_ADD_OUTPUT:
            if(PACKET_Long(msg) == PACKET_LONG_DONE)
                Bitcoin_Add_Output();
            break;
        case BITCOIN_FUNC_SIGN_TX:
            Bitcoin_Tx_Sign();
            break;
        default:
            PACKET_SendNAK();
    };
}
